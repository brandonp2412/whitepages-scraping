const fetch = require("node-fetch");
const jsdom = require("jsdom");
const fs = require("fs");

const main = async () => {
  for (let page = 1; page < 63; page++) {
    const res = await fetch(
      `https://whitepages.co.nz/white-all/.*/red-beach/${page}/`
    );
    const resText = await res.text();

    const dom = new jsdom.JSDOM(resText);
    const document = dom.window.document;

    const addresses = document.querySelectorAll(
      "div > div.itemNameLocation > div > p > a"
    );
    const phoneNumbers = document.querySelectorAll(
      "div > div.itemTools > div > a.button.standard.phoneLink"
    );

    for (let link = 0; link < addresses.length; link++) {
      const address = addresses[link].innerHTML.trim();
      const phoneNumber = phoneNumbers[link].getAttribute("data-telephone");
      fs.appendFileSync(
        "contacts-red-beach.csv",
        `"${address}",${phoneNumber}\n`,
        "utf8",
        () => {}
      );
    }
    console.log(`Wrote page #${page}`);
  }
};

main();